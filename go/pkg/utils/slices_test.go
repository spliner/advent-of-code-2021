package utils_test

import (
	"aoc2021/pkg/utils"
	"reflect"
	"testing"
)

func TestWindowed(t *testing.T) {
	t.Run("windowed of integers", func(t *testing.T) {
		input := []int{1, 2, 3, 4, 5}

		got := utils.Windowed(input, 3)
		want := [][]int{
			{1, 2, 3},
			{2, 3, 4},
			{3, 4, 5},
		}

		if !reflect.DeepEqual(got, want) {
			t.Fatalf("got %v want %v", got, want)
		}
	})

	t.Run("size greater than length returns empty", func(t *testing.T) {
		input := []int{1, 2, 3}

		got := utils.Windowed(input, 5)
		want := [][]int{}

		if !reflect.DeepEqual(got, want) {
			t.Fatalf("got %v want %v", got, want)
		}
	})
}

func TestSum(t *testing.T) {
	t.Run("sum of ints", func(t *testing.T) {
		input := []int{1, 2, 3, 4, 5}

		got := utils.Sum(input)
		want := 15

		if got != want {
			t.Fatalf("got %d want %d", got, want)
		}
	})

	t.Run("sum of floats", func(t *testing.T) {
		input := []float64{1, 2, 3, 4, 5}

		got := utils.Sum(input)
		want := 15.0

		if got != want {
			t.Fatalf("got %f want %f", got, want)
		}
	})
}
