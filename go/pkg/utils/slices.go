package utils

func Windowed[T any](slice []T, size int) [][]T {
	windows := [][]T{}

	for i := 0; i+size <= len(slice); i++ {
		window := slice[i : size+i]
		windows = append(windows, window)
	}

	return windows
}

func Sum[T int | int8 | int32 | int64 | float32 | float64](slice []T) T {
	var sum T

	for _, item := range slice {
		sum += item
	}

	return sum
}
