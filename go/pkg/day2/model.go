package day2

type Submarine struct {
	Position int
	Depth    int
	Aim      int
}

type Command interface {
	Apply(*Submarine)
}

type PartOneForwardCommand struct {
	Unit int
}

func (c *PartOneForwardCommand) Apply(s *Submarine) {
	s.Position += c.Unit
}

type PartOneDownCommand struct {
	Unit int
}

func (c *PartOneDownCommand) Apply(s *Submarine) {
	s.Depth += c.Unit
}

type PartOneUpCommand struct {
	Unit int
}

func (c *PartOneUpCommand) Apply(s *Submarine) {
	s.Depth -= c.Unit
}

type PartTwoDownCommand struct {
	Unit int
}

func (c *PartTwoDownCommand) Apply(s *Submarine) {
	s.Aim += c.Unit
}

type PartTwoUpCommand struct {
	Unit int
}

func (c *PartTwoUpCommand) Apply(s *Submarine) {
	s.Aim -= c.Unit
}

type PartTwoForwardCommand struct {
	Unit int
}

func (c *PartTwoForwardCommand) Apply(s *Submarine) {
	s.Position += c.Unit
	s.Depth += s.Aim * c.Unit
}
