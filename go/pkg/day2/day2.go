package day2

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

type commandFactory func(unit int) Command

func Part1(input string) (int, error) {
	return run(
		input,
		func(i int) Command { return &PartOneForwardCommand{i} },
		func(i int) Command { return &PartOneDownCommand{i} },
		func(i int) Command { return &PartOneUpCommand{i} })
}

func Part2(input string) (int, error) {
	return run(
		input,
		func(i int) Command { return &PartTwoForwardCommand{i} },
		func(i int) Command { return &PartTwoDownCommand{i} },
		func(i int) Command { return &PartTwoUpCommand{i} },
	)
}

func run(
	input string,
	forwardCommandFactory commandFactory,
	downCommandFactory commandFactory,
	upCommandFactory commandFactory) (int, error) {
	commands, err := parseInput(
		input,
		forwardCommandFactory,
		downCommandFactory,
		upCommandFactory)
	if err != nil {
		return 0, err
	}

	submarine := &Submarine{}
	result := ApplyCommands(submarine, commands)

	return result, nil
}

func parseInput(
	input string,
	forwardCommandFactory commandFactory,
	downCommandFactory commandFactory,
	upCommandFactory commandFactory) ([]Command, error) {
	lines := strings.Split(strings.TrimSpace(input), "\n")
	commands := make([]Command, len(lines))

	for i, line := range lines {
		parts := strings.Split(line, " ")
		if len(parts) != 2 {
			return nil, errors.New("expected line to have a single space")
		}

		unit, err := strconv.Atoi(strings.TrimSpace(parts[1]))
		if err != nil {
			return nil, err
		}

		commandName := parts[0]
		var command Command
		if commandName == "forward" {
			command = forwardCommandFactory(unit)
		} else if commandName == "down" {
			command = downCommandFactory(unit)
		} else if commandName == "up" {
			command = upCommandFactory(unit)
		}

		if command == nil {
			fmt.Println("null command")
			return nil, errors.New("unable to parse command name")
		}

		commands[i] = command
	}

	return commands, nil
}

func ApplyCommands(submarine *Submarine, commands []Command) int {
	for _, command := range commands {
		command.Apply(submarine)
	}

	return submarine.Depth * submarine.Position
}
