package day2_test

import (
	"aoc2021/pkg/day2"
	"testing"
)

func TestApplyCommands(t *testing.T) {
	t.Run("part 1", func(t *testing.T) {
		submarine := &day2.Submarine{}

		commands := []day2.Command{
			&day2.PartOneForwardCommand{Unit: 5},
			&day2.PartOneDownCommand{Unit: 5},
			&day2.PartOneForwardCommand{Unit: 8},
			&day2.PartOneUpCommand{Unit: 3},
			&day2.PartOneDownCommand{Unit: 8},
			&day2.PartOneForwardCommand{Unit: 2},
		}

		got := day2.ApplyCommands(submarine, commands)
		want := 150

		if got != want {
			t.Fatalf("got %d want %d", got, want)
		}
	})

	t.Run("part 2", func(t *testing.T) {
		submarine := &day2.Submarine{}

		commands := []day2.Command{
			&day2.PartTwoForwardCommand{Unit: 5},
			&day2.PartTwoDownCommand{Unit: 5},
			&day2.PartTwoForwardCommand{Unit: 8},
			&day2.PartTwoUpCommand{Unit: 3},
			&day2.PartTwoDownCommand{Unit: 8},
			&day2.PartTwoForwardCommand{Unit: 2},
		}

		got := day2.ApplyCommands(submarine, commands)
		want := 900

		if got != want {
			t.Fatalf("got %d want %d", got, want)
		}
	})
}
