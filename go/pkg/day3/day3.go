package day3

import (
	"fmt"
	"strconv"
	"strings"
)

func Part1(input string) (int, error) {
	report, err := ParseInput(input)
	if err != nil {
		return 0, err
	}

	consumption, err := PowerConsumption(report)
	if err != nil {
		return 0, err
	}

	return consumption, nil
}

func ParseInput(input string) ([][]int, error) {
	input = strings.TrimSpace(input)
	lines := strings.Split(input, "\n")
	report := make([][]int, len(lines))
	for i, line := range lines {
		numbers := strings.Split(line, "")
		foo := make([]int, len(numbers))
		for j, n := range numbers {
			bar, err := strconv.ParseInt(n, 2, 8)
			if err != nil {
				return nil, err
			}

			foo[j] = int(bar)
		}
		report[i] = foo
	}

	return report, nil
}

func PowerConsumption(report [][]int) (int, error) {
	gamma, epsilon := rates(report)
	parsedGamma, err := strconv.ParseInt(gamma, 2, 32)
	if err != nil {
		return 0, err
	}

	parsedEpsilon, err := strconv.ParseInt(epsilon, 2, 32)
	if err != nil {
		return 0, err
	}

	return int(parsedGamma) * int(parsedEpsilon), nil
}

func rates(report [][]int) (string, string) {
	gammaRate := ""
	epsilonRate := ""
	for i := 0; i < len(report[0]); i++ {
		counts := make(map[int]int)
		for j := 0; j < len(report); j++ {
			value := report[j][i]
			count := counts[value]
			counts[value] = count + 1
		}

		mostCommon, leastCommon := getOrderedKeys(counts)
		gammaRate += fmt.Sprint(mostCommon)
		epsilonRate += fmt.Sprint(leastCommon)
	}

	return gammaRate, epsilonRate
}

func getOrderedKeys(counts map[int]int) (int, int) {
	mostCommonKey := 0
	for key, count := range counts {
		if count > counts[mostCommonKey] {
			mostCommonKey = key
		}
	}

	var leastCommonKey int
	if mostCommonKey == 1 {
		leastCommonKey = 0
	} else {
		leastCommonKey = 1
	}

	return mostCommonKey, leastCommonKey
}
