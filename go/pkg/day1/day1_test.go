package day1

import (
	"reflect"
	"testing"
)

func TestCountDepthIncreases(t *testing.T) {
	assertCorrectMessage := func(t testing.TB, got, want int) {
		t.Helper()
		if got != want {
			t.Errorf("got %d want %d", got, want)
		}
	}

	t.Run("1 depth increase", func(t *testing.T) {
		depths := []int{
			100,
			101,
			99,
			98,
			97,
		}

		count := countDepthIncreases(depths)

		assertCorrectMessage(t, count, 1)
	})

	t.Run("5 depth increase2", func(t *testing.T) {
		depths := []int{
			100,
			99,
			100,
			101,
			100,
			101,
			102,
			103,
		}

		count := countDepthIncreases(depths)

		assertCorrectMessage(t, count, 5)
	})
}

func TestParseInput(t *testing.T) {
	t.Run("parsing valid input", func(t *testing.T) {
		input := `1
20
100
999`
		depths, err := parseInput(input)

		if err != nil {
			t.Fatal("expected no error:", err)
		}

		want := []int{1, 20, 100, 999}

		if !reflect.DeepEqual(depths, want) {
			t.Fatalf("got %v want %v", depths, want)
		}
	})

	t.Run("parsing invalid input", func(t *testing.T) {
		input := `1
a`

		depths, err := parseInput(input)

		if err == nil {
			t.Fatal("expected error, got nil")
		}

		if depths != nil {
			t.Fatalf("expected nil depths, got %v", depths)
		}
	})
}

func TestCountWindowedDepthIncreases(t *testing.T) {
	input := [][]int{
		{199, 200, 208},
		{200, 208, 210},
		{208, 210, 200},
		{210, 200, 207},
		{200, 207, 240},
		{207, 240, 269},
		{240, 269, 260},
		{269, 260, 263},
	}

	got := countWindowedDepthIncreases(input)
	want := 5

	if got != want {
		t.Fatalf("got %d want %d", got, want)
	}
}
