package day1

import (
	"aoc2021/pkg/utils"
	"strconv"
	"strings"
)

func Part1(input string) (int, error) {
	depths, err := parseInput(input)
	if err != nil {
		return 0, err
	}

	depthIncreases := countDepthIncreases(depths)
	return depthIncreases, nil
}

func Part2(input string) (int, error) {
	depths, err := parseInput(input)
	if err != nil {
		return 0, err
	}

	windowedDepths := utils.Windowed(depths, 3)
	count := countWindowedDepthIncreases(windowedDepths)

	return count, nil
}

func parseInput(input string) ([]int, error) {
	lines := strings.Fields(input)
	depths := make([]int, len(lines))

	for i, line := range lines {
		depth, err := strconv.Atoi(strings.TrimSpace(line))
		if err != nil {
			return nil, err
		}

		depths[i] = depth
	}

	return depths, nil
}

func countDepthIncreases(depths []int) int {
	var previousDepth int
	var count int
	for i, depth := range depths {
		if i == 0 {
			previousDepth = depth
			continue
		}

		if depth > previousDepth {
			count++
		}

		previousDepth = depth
	}

	return count
}

func countWindowedDepthIncreases(windowedDepths [][]int) int {
	previousSum := 0
	count := 0
	for i, depths := range windowedDepths {
		depthSum := utils.Sum(depths)
		if i == 0 {
			previousSum = depthSum
			continue
		}

		if depthSum > previousSum {
			count++
		}

		previousSum = depthSum
	}

	return count
}
