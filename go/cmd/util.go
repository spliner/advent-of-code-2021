package cmd

import (
	"os"
)

func readInput(path string) (string, error) {
	input, err := os.ReadFile(path)
	if err != nil {
		return "", err
	}

	return string(input), nil
}
