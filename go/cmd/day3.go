package cmd

import (
	"aoc2021/pkg/day3"
	"errors"
	"fmt"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(day3Command)
}

var day3Command = &cobra.Command{
	Use:   "day3 [input path] [part]",
	Short: "Day 3 solution",
	RunE: func(cmd *cobra.Command, args []string) error {
		input, err := readInput(args[0])
		if err != nil {
			return err
		}

		part := args[1]
		if part == "1" {
			result, err := day3.Part1(input)
			if err != nil {
				return err
			}

			fmt.Printf("Day 3 part 1: %d\n", result)
		} else if part == "2" {
			fmt.Println("TBD")
		} else {
			return errors.New(fmt.Sprintf("invalid part: %s", part))
		}

		return nil
	},
}
