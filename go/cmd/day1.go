package cmd

import (
	"aoc2021/pkg/day1"
	"errors"
	"fmt"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(day1Command)
}

var day1Command = &cobra.Command{
	Use:   "day1 [input path] [part]",
	Short: "Day 1 solution",
	RunE: func(cmd *cobra.Command, args []string) error {
		input, err := readInput(args[0])
		if err != nil {
			return err
		}

		part := args[1]
		if part == "1" {
			result, err := day1.Part1(input)
			if err != nil {
				return err
			}

			fmt.Printf("Day 1 part 1: %d\n", result)
		} else if part == "2" {
			result, err := day1.Part2(input)
			if err != nil {
				return err
			}

			fmt.Printf("Day 1 part 2: %d\n", result)
		} else {
			return errors.New(fmt.Sprintf("invalid part: %s", part))
		}

		return nil
	},
}
